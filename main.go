package main

import (
	"log"
	"net/http"
	"os"

	"github.com/google/uuid"
)

var clients = make(map[string]chan string)

// handshake handle all http requests made to make a "handshake" (https://es.wikipedia.org/wiki/Establecimiento_de_comunicaci%C3%B3n)
func handshake(w http.ResponseWriter, r *http.Request) {
	// SSE (Server Sent Events)
	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")

	// CORS
	w.Header().Set("Access-Control-Allow-Origin", "*")

	index := uuid.New().String()

	clients[index] = make(chan string)

	log.Printf("Client connected with id '%v'\n", index)

	// Delete client channel by index
	defer delete(clients, index)

	// Obtains Flusher to send buffered data to the client
	flusher, _ := w.(http.Flusher)

	for {
		select {
		case message := <-clients[index]:
			log.Printf(`Client "%v" received the follow message "%v" %v`, index, message, "\n")
			w.Write([]byte(message))
			flusher.Flush()

		case <-r.Context().Done():
			return
		}
	}
}

// message handle all request to dispatch a event to connected clients
func message(w http.ResponseWriter, r *http.Request) {
	message := r.URL.Query().Get("message")
	if message == "" {
		w.Write([]byte(`Query param "message" is required` + "\n"))
		return
	}

	for _, client := range clients {
		client <- message
	}

	w.Write([]byte("The message received was sent to all connected clients\n"))
}

const defaultPort = "8080"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	log.SetFlags(log.Flags() | log.Lshortfile)

	http.HandleFunc("/handshake", handshake)

	http.HandleFunc("/send", message)

	log.Printf(`http server is running on port "%v" %v`, port, "💀\n")
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
