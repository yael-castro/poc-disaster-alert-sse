# poc-disaster-alert-sse

This repository contains a "Proof of Concept" for "Server Sent Events" to be used later in a future feature to disaster alert api.
The function will consist of a real-time notification system for disaster alerts that will allow integration with government entities
so that they can act faster in the event of a disaster.

###### How to use

First: start the server
```
$ go run main.go &
```

Second: make a client connects to the server using curl (Hanshake)
```
$ curl "locahost:8080/handshake" &
```

And for the last: make a request to send a message to all connected clients (Server sends an event)
```
$ curl "locahost:8080/send?message=hello"
```

###### Server sent events (Infographics)
![Infographic](docs/images/sse.png)

###### Notes

* Every time that a client connects to the server a uuid is
  generated to be used as client id and channel is created
  to maintain a kind of session

* Every time that a client connects to the server a uuid is
  generated to be used as client id and channel is created to maintained
  a kind of session

* Every time a client disconnects from the server a previously created client id is deleted
  and at the same time the session is removed (the channel previously
  assigned to the client is deleted)

* The server does not have a "rate limiting"

*  The server does not have a limit connections for user

###### References
* [Hanshake](https://es.wikipedia.org/wiki/Establecimiento_de_comunicaci%C3%B3n)
* [Server side events first steps](https://developer.mozilla.org/es/docs/Learn/Server-side/First_steps/Client-Server_overview)

